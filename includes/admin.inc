<?php

/**
 * @file
 * Contains the administrative functions of the comScore Tracking code module.
 *
 * This file is included by the comScore Tracking code, and includes the
 * settings form.
 */

/**
 * Implements hook_admin_settings_form().
 * Used to create the admin form to configure the comScore Tracking script.
 */
function comscore_tracking_code_form($form, &$form_state) {
  $form['comscore_id'] = array(
    '#type' => 'textfield',
    '#title' => t('comSCore Client ID'),
    '#maxlength' => 7,
    '#size' => 7,
    '#required' => TRUE,
    '#default_value' => variable_get('comscore_id', '1234567'),
  );

  return system_settings_form($form);
}
